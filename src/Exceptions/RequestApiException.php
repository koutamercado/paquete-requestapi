<?php

namespace koutamercado\RequestApi\Exceptions;

class RequestApiException extends \Exception
{
    /**
     * @var array
     */
    public $data = [];

    public function __construct($message, $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param mixed $data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
