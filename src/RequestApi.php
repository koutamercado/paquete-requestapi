<?php

namespace koutamercado\RequestApi;

use \Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use koutamercado\RequestApi\Exceptions\RequestApiException;

class RequestApi
{
    /**
     * @var Object
     */
    private $client;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var array
     */
    private $methods = [
        "get"    => "GET",
        "post"   => "POST",
        "put"    => "PUT",
        "patch"   => "PATCH",
        "delete" => "delete",
    ];

    /**
     * @var array
     */
    private $options = [];

    /**
     * @param string  $base_uri
     * @param boolean $debug
     *
     * @return void
     */
    public function __construct($base_uri, $debug = false)
    {
        $this->client = new Client([
            'debug'    => $debug,
            'base_uri' => $base_uri,
        ]);
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @return void
     */
    public function setHeaders($name, $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function exec(array $options)
    {
        $uri    = $this->getUri($options);
        $method = $this->getMethod($options);
        $data   = $this->parseData($options);

        $dataResponse = [];
        try {
            $response = $this->client->request($method, $uri, $data);

            $dataResponse['status']  = (string)$response->getStatusCode();
            $dataResponse['message'] = (string)$response->getReasonPhrase();
            $dataResponse["data"]    = (string)$response->getBody();
        } catch (ClientException $e) {
            $dataResponse['status']  = (string)$e->getResponse()->getStatusCode();
            $dataResponse['message'] = (string)$e->getResponse()->getReasonPhrase();
            $dataResponse['data']    = $e->hasResponse() ? (string)$e->getResponse()->getBody() : null;
        } catch (ServerException $e) {
            $dataResponse['status']  = (string)$e->getResponse()->getStatusCode();
            $dataResponse['message'] = (string)$e->getResponse()->getReasonPhrase();
            $dataResponse['data']    = $e->hasResponse() ? (string)$e->getResponse()->getBody() : null;
        } catch (ConnectException $e) {
            $dataResponse['status']  = (string)$e->getCode();
            $dataResponse['message'] = (string)$e->getMessage();
            $dataResponse['data']    = $e->hasResponse() ? (string)$e->getResponse()->getBody() : null;
        } catch (Exception $e) {
            $dataResponse['status']    = $e->getCode();
            $dataResponse['message']   = $e->getMessage();
            $dataResponse['data']      = null;
            $dataResponse['Exception'] = get_class($e);
        }

        return $dataResponse;
    }

    /**
     * @param  string $uri
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function post($uri, array $options = [])
    {
        $options['method'] = 'post';
        $options['uri']    = $uri;

        return $this->exec($options);
    }

    /**
     * @param  string $uri
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function get($uri, array $options = [])
    {
        $options['method'] = 'get';
        $options['uri']    = $uri;
        
        return $this->exec($options);
    }

    /**
     * @param  string $uri
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function put($uri, array $options = [])
    {
        $options['method'] = 'put';
        $options['uri']    = $uri;
        
        return $this->exec($options);
    }

    /**
     * @param  string $uri
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function patch($uri, array $options = [])
    {
        $options['method'] = 'patch';
        $options['uri']    = $uri;
        
        return $this->exec($options);
    }

    /**
     * @param  string $uri
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return array
     */
    public function delete($uri, array $options = [])
    {
        $options['method'] = 'delete';
        $options['uri']    = $uri;
        
        return $this->exec($options);
    }

    /**
     * @param  array  $options
     * @return array
     */
    private function parseData(array $options)
    {
        $data = [];

        if (isset($options['query']) && is_array($options['query']) && !empty($options['query'])) {
            $data['query'] = $options['query'];
        }

        if (isset($options['body']) && is_array($options['body']) && !empty($options['body'])) {
            $data['form_params'] = $options['body'];
        }

        if (!empty($this->getHeaders())) {
            $data['headers'] = $this->getHeaders();
        }

        return $data;
    }

    /**
     * @param  array  $options
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return string
     */
    private function getMethod(array $options)
    {
        if (!isset($options['method'])) {
            throw new RequestApiException("method not set");
        }

        if (trim($options['method']) == '') {
            throw new RequestApiException("method can't be empty");
        }

        if (!array_key_exists($options['method'], $this->methods)) {
            throw new RequestApiException("Method not found '$method'");
        }

        return $this->methods[$options['method']];
    }

    /**
     * @param  array  $options [description]
     *
     * @throws koutamercado\RequestApi\Exceptions\RequestApiException
     * @return string
     */
    private function getUri(array $options)
    {
        if (!isset($options['uri'])) {
            throw new RequestApiException("URI not set");
        }

        if (trim($options['uri']) == '') {
            throw new RequestApiException("URI can't be empty");
        }

        return $options['uri'];
    }
}
